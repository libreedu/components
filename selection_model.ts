// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: LGPL-3.0-or-later

import { Signal, DebounceSignal } from './signals.ts';

/**
 * A selection model is a simple interface to add support for selections
 * to a list model.
 *
 * Adwaita provides the most common selection modes like GTK; see, e.g.,
 * {@link MultiSelection}.
 *
 * GtkSelectionModel looked overcomplicated at first glance, so this
 * exists now.
 *
 * @see {@link MultiSelection}
 * @see {@link NoSelection}
 * @see {@link SingleSelection}
 *
 * @typeParam I What type the identifiers are.
 */
export abstract class SelectionModel<
  I extends string | number | symbol,
> {
  public model: Record<I, boolean>;

  /**
   * @param s The item to deselect
   * @returns Whether the item was selected
   * @emits {@link SelectionModel.selectionChanged}
   */
  public abstract select(s: I): boolean;

  /**
   * @param s The item to deselect
   * @returns Whether the item was deselected
   * @emits {@link SelectionModel.selectionChanged}
   */
  public abstract deselect(s: I): boolean;

  /** @returns Whether _all_ items were successfully selected */
  public selectAll(): boolean {
    this.getSelection().forEach(this.deselect);
    return Object.keys(this.model).length == this.getSelection().length;
  }

  /** @returns Whether _all_ items were successfully deselected */
  public deselectAll(): boolean {
    this.getSelection().forEach(this.deselect);
    return this.getSelection().length == 0;
  }

  /** @param s @returns Whether `s` is selected */
  public isSelected(s: I): boolean {
    return this.model[s];
  }

  public getSelection(): I[] {
    const i: I[] = [];
    for (const n in this.model) {
      if (this.isSelected(n)) i.push(n);
    }

    return i;
  }

  /**
   * Emitted when the selection state of some of the items in
   * {@link SelectionModel.model} changes.
   *
   * Debounces for 10ms.
   */
  public selectionChanged: Signal<[]> = new DebounceSignal<[]>(10);

  /** @param m The model to use. If not provided, `{}` is used. */
  public constructor(m: Record<I, boolean>) {
    this.model = m || <Record<I, boolean>> {};
  }
}

export class MultiSelection<
  I extends string | number | symbol,
> extends SelectionModel<I> {
  public select(s: I) {
    this.model[s] = true;
    this.selectionChanged.fire();
    return true;
  }

  public deselect(s: I) {
    this.model[s] = false;
    this.selectionChanged.fire();
    return true;
  }
}

export class NoSelection<
  I extends string | number | symbol,
> extends SelectionModel<I> {
  declare public model: Record<I, false>;

  public select(_: I) {
    return false;
  }

  public deselect(_: I) {
    return true;
  }

  public isSelected() {
    return false;
  }
}

export class SingleSelection<
  I extends string | number | symbol,
> extends SelectionModel<I> {
  public select(s: I) {
    this.deselectAll();
    this.model[s] = true;
    this.selectionChanged.fire();
    return true;
  }

  public deselect(s: I) {
    if (this.model[s]) {
      throw new Error('Tried to deselect single selection');
    }
    return true;
  }
}
