// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: LGPL-3.0-or-later

import { DebounceSignal, DOMSignal, Signal } from "./signals.ts";

import {
  assert,
  assertEquals,
  assertThrows,
  fail,
} from "https://deno.land/std@0.148.0/testing/asserts.ts";

async function test(s) {
  const cb = () => fail(".disconnect() failed");
  s.connect((s) => s.disconnect(cb));
  s.connect(cb);
  s.connect((s) => s.stopEmission());
  s.connect(() => fail(".stopEmission() failed"));
  s.fire(s);

  // FIXME: testing .once() automatically

  // works in repl
  // > (async () => {await s.once(); console.log('done')})()
  // > ...
  // > s.fire()
  // done
  // undefined
}

Deno.test("Signal", async () => await test(new Signal()));
Deno.test("DebounceSignal", () => {
  const s = new DebounceSignal(10);
  test(s);
  assertThrows(() => s.fire(s), Error, "Debounce violation");
});
Deno.test("DOMSignal", () => {
  const et = new EventTarget();
  const s = new DOMSignal(et, "eventName");
  let count = 0;
  const inc = () => count++;
  s.connect(inc);
  et.dispatchEvent(new Event("eventName"));
  assertEquals(count, 1, "signal callback not being called by DOM target");
  et.dispatchEvent(new Event("otherEventName"));
  assertEquals(count, 1, "signal picking up separate DOM events");
  s.disconnect(inc);
  et.addEventListener("eventName", inc);
  s.fire(new Event("eventName"));
  assertEquals(count, 2, "DOM listener not being called by signal");
});
