// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: LGPL-3.0-or-later

/** @file @todo MultiSelection tests */

import {
  MultiSelection,
  NoSelection,
  SingleSelection,
} from './selection_model.ts';
import {
  assert,
  assertThrows,
} from 'https://deno.land/std@0.148.0/testing/asserts.ts';

Deno.test('MultiSelection', () => {});

Deno.test('NoSelection', () => {
  const sel = new NoSelection({ abc: false, xyz: false });
  assert(sel.deselect('abc'), 'deselect failed');
  assert(!sel.select('xyz'), 'select succeeded');
  assert(sel.getSelection().length == 0, 'values were selected');
});

Deno.test('SingleSelection', () => {
  const sel = new SingleSelection({ abc: false, xyz: false });
  sel.select('xyz');
  const gs = sel.getSelection();
  assert(gs.length == 1, 'selection length is not 1');
  assert(gs[0] == 'xyz', 'selected item was not `xyz`');

  assert(sel.deselect('abc'));

  assertThrows(
    () => sel.deselect('xyz'),
    Error,
    undefined,
    'succeeded deselecting single selection',
  );
});
