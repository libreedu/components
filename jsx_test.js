// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: LGPL-3.0-or-later

import { DOMParser } from "https://esm.sh/linkedom";
import {
  assert,
  assertEquals,
} from "https://deno.land/std@0.132.0/testing/asserts.ts";
import { createElement, createFragment } from "./jsx.ts";

// @ts-ignore
window.document = new DOMParser().parseFromString(
  `<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hello from Deno</title>
  </head>
  <body></body>
</html>`,
  "text/html",
);
assert(document);

Deno.test("jsx element factory", () => {
  assertEquals(
    createElement("p", {}, "Hello", createElement("code", {}, "world"))
      .nodeName,
    "P",
  );
});

Deno.test("jsx fragment factory", () => {
  const f = createFragment(
    {},
    createElement(
      "p",
      { className: "one" },
      "Hello ",
      createElement("code", {}, "world"),
    ),
  );
  document.body.append(f);
  assertEquals(document.body.lastChild.lastChild.nodeName, "CODE");
});
