// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: LGPL-3.0-or-later

export type SignalCallback<AT extends unknown[]> = (
  ...args: AT
) => void;

export interface ISignal<AT extends unknown[]> {
  emit(...args: AT): void;
  fire(...args: AT): void;
  stopEmission?(): void;
  connect(callback: SignalCallback<AT>): void;
  disconnect(callback: SignalCallback<AT>): void;
  once(): Promise<AT>;
}

/**
 * A class vaguely implementing Vala (GLib) signals. Not compatible with
 * Node.js's `EventEmitter`.
 *
 * @typeParam AT The argument types for callbacks as a tuple, e.g.
 * `<[string, number]>`, or with named function parameters,
 * `<[x: string, y: number]>`. These ought to be documented in the
 * property description.
 */
export class Signal<AT extends unknown[]> implements ISignal<AT> {
  /**
   * This constructor is meant to be used by class implementations,
   * typically in their instance init function.
   */
  public constructor() {
    this.callbacks = [];
  }

  private callbacks: SignalCallback<AT>[];

  /**
   * Emits a signal.
   *
   * Signal emission is done synchronously. The method will only
   * return after all handlers are called or signal emission was
   * stopped.
   */
  public emit(...args: AT) {
    for (let i = 0; i < this.callbacks.length && !this.stop; i++) {
      this.callbacks[i](...args);
    }

    this.stop = false;
  }

  /**
   * @alias
   * @see {@link Signal.emit} */
  public fire(...args: AT) {
    this.emit(...args);
  }

  /**
   * Stops a signal's current emission.
   *
   * Prints a warning if used on a signal which isn't being emitted.
   */
  public stopEmission() {
    this.stop = true;
  }
  private stop = false;

  /** Callers: copy objects in the callback. */
  public connect(callback: SignalCallback<AT>) {
    this.callbacks.push(callback);
  }

  public disconnect(callback: SignalCallback<AT>) {
    this.callbacks = this.callbacks.filter((c) => c != callback);
  }

  public once() {
    return new Promise<AT>((resolve) => {
      const l = (...args: AT) => (this.disconnect(l), resolve(args));
      this.connect(l);
    });
  }
}

export class DebounceSignal<AT extends unknown[]> extends Signal<AT> {
  constructor(private ms: number) {
    super();
  }

  private debounce = 0;

  public emit(...args: AT) {
    const date = Date.now();
    if (this.debounce + 10 >= date) {
      throw new Error("Debounce violation");
    }
    this.debounce = date;
    super.emit(...args);
  }
}

export interface DOMEventEmitter<AT extends unknown[]> {
  addEventListener(k: string, f: (...args: AT) => any): any;
  removeEventListener?(k: string, f: (...args: AT) => any): any;
  dispatchEvent(...args: AT): boolean;
}
const _: DOMEventEmitter<[Event]> = new EventTarget();

export class DOMSignal<AT extends unknown[]> extends Signal<AT> {
  constructor(private ee: DOMEventEmitter<AT>, name: string) {
    super();
    ee.addEventListener(name, (...args: AT) => super.emit(...args));
  }

  public emit(...args: AT) {
    this.ee.dispatchEvent(...args);
  }
}
