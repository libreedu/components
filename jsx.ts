// SPDX-FileCopyrightText: Copyright (c) 2022 Alexander Noble
//
// SPDX-License-Identifier: LGPL-3.0-or-later

const attributeMap: Record<string, string> = {
  className: "class",
  htmlFor: "for",
};

type Props = Record<string, any>;

export function createElement(
  tag: string | Function,
  props: Props,
  ...children: Node[]
): Node {
  if (typeof tag === "function") return tag(props, ...children);
  const element = document.createElement(tag);

  Object.entries(props || {}).forEach(([name, value]) => {
    if (name.startsWith("on") && name.toLowerCase() in window) {
      element.addEventListener(name.toLowerCase().substr(2), value);
    } else if (attributeMap.hasOwnProperty(name)) {
      element.setAttribute(attributeMap[name], value.toString());
    } else element.setAttribute(name, value.toString());
  });

  children.forEach((child) => {
    appendChild(element, child);
  });

  return element;
}

export function createFragment(props: Props, ...children: Node[]): Node {
  const fragment = document.createDocumentFragment();
  children.forEach((c) => fragment.append(c));
  return fragment;
}

function appendChild(parent: Node, child: string | Node): void {
  if (Array.isArray(child)) {
    child.forEach((nestedChild) => appendChild(parent, nestedChild));
  } else {
    parent.appendChild(
      (<Node> child).nodeType
        ? <Node> child
        : document.createTextNode(<string> child),
    );
  }
}
